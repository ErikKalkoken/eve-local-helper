"""Handle fetching and processing characters from ESI."""

import concurrent.futures
import datetime as dt
import logging
from dataclasses import dataclass
from typing import Dict, Iterable, List, Optional, Set, Tuple

from esi_client.apis import CharacterApi
from esi_client.models import GetCharactersCharacterIdOk, PostCharactersAffiliation200Ok

from eve_local_helper import entities, eveimageserver
from eve_local_helper.cache import Cache

logger = logging.getLogger(__name__)

ZKB_BASE_URL = "https://zkillboard.com"
CHARACTER_DETAIL_TIMEOUT = 60 * 60 * 12  # 12 hours


@dataclass
class CharacterAffiliation:
    character_id: int
    corporation_id: int
    alliance_id: Optional[int]
    faction_id: Optional[int]

    @classmethod
    def from_esi_record(
        cls, esi_record: PostCharactersAffiliation200Ok
    ) -> "CharacterAffiliation":
        try:
            alliance_id = esi_record.alliance_id
        except AttributeError:
            alliance_id = None
        try:
            faction_id = esi_record.faction_id
        except AttributeError:
            faction_id = None
        return cls(
            character_id=esi_record.character_id,
            corporation_id=esi_record.corporation_id,
            alliance_id=alliance_id,
            faction_id=faction_id,
        )


@dataclass
class CharacterDetail:
    birthday: dt.datetime
    security_status: float

    @classmethod
    def from_esi_character(cls, esi_character: GetCharactersCharacterIdOk):
        birthday = esi_character.birthday
        try:
            security_status = esi_character.security_status
        except AttributeError:
            security_status = 0.0
        return cls(birthday=birthday, security_status=security_status)


@dataclass
class Character:
    id: int
    corporation_id: int
    birthday: Optional[dt.datetime] = None
    security_status: Optional[float] = None
    name: str = ""
    corporation_name: str = ""
    alliance_id: Optional[int] = None
    alliance_name: str = ""
    faction_id: Optional[int] = None
    faction_name: str = ""

    @property
    def has_alliance(self) -> bool:
        return self.alliance_id is not None

    @property
    def has_faction(self) -> bool:
        return self.faction_id is not None

    def entity_ids(self) -> Set[int]:
        result = {self.id, self.corporation_id}
        if self.alliance_id:
            result.add(self.alliance_id)
        if self.faction_id:
            result.add(self.faction_id)
        return result

    def character_portrait_url(self, size: int) -> str:
        return eveimageserver.character_portrait_url(self.id, size)

    def corporation_logo_url(self, size: int) -> str:
        return eveimageserver.corporation_logo_url(self.corporation_id, size)

    def alliance_logo_url(self, size: int) -> str:
        if not self.alliance_id:
            return ""

        return eveimageserver.alliance_logo_url(self.alliance_id, size)

    def faction_logo_url(self, size: int) -> str:
        if not self.faction_id:
            return ""

        return eveimageserver.faction_logo_url(self.faction_id, size)

    def character_zkb_url(self) -> str:
        return f"{ZKB_BASE_URL}/character/{self.id}/"

    def corporation_zkb_url(self) -> str:
        return f"{ZKB_BASE_URL}/corporation/{self.corporation_id}/"

    def alliance_zkb_url(self) -> str:
        return (
            f"{ZKB_BASE_URL}/alliance/{self.alliance_id}/" if self.alliance_id else ""
        )

    def faction_zkb_url(self) -> str:
        return f"{ZKB_BASE_URL}/faction/{self.faction_id}/" if self.faction_id else ""

    def update_names_from_map(self, entities: Dict[int, str]):
        if self.id in entities:
            self.name = entities[self.id]
        if self.corporation_id in entities:
            self.corporation_name = entities[self.corporation_id]
        if self.alliance_id and self.alliance_id in entities:
            self.alliance_name = entities[self.alliance_id]
        if self.faction_id and self.faction_id in entities:
            self.faction_name = entities[self.faction_id]

    def update_from_character_details(self, detail: CharacterDetail):
        self.birthday = detail.birthday
        self.security_status = detail.security_status

    @classmethod
    def from_affiliation(cls, affiliation: CharacterAffiliation) -> "Character":
        return cls(
            id=affiliation.character_id,
            corporation_id=affiliation.corporation_id,
            alliance_id=affiliation.alliance_id,
            faction_id=affiliation.faction_id,
        )


def fetch_characters_from_names(names: List[str]) -> Tuple[List[Character], List[str]]:
    """Fetch characters from names with caching."""
    if not names:
        return [], []

    character_ids, unresolved_names = entities.resolve_names_to_character_ids(names)
    if not character_ids:
        return [], unresolved_names

    characters = create_characters_from_ids(character_ids)
    _add_names_to_characters(characters)
    _add_details_to_characters(characters)
    characters.sort(key=lambda o: o.name)
    return characters, unresolved_names


def create_characters_from_ids(character_ids: List[int]) -> List[Character]:
    """Create characters from character IDs."""
    characters = [
        Character.from_affiliation(o)
        for o in fetch_affiliation_for_characters(character_ids)
    ]
    return characters


def _add_names_to_characters(characters: Iterable[Character]):
    # get names for all ids
    ids = set()
    for character in characters:
        ids |= character.entity_ids()
    entities_map = entities.resolve_entity_ids(ids)

    # update names
    for character in characters:
        character.update_names_from_map(entities_map)


def _add_details_to_characters(characters: Iterable[Character]):
    def make_cache_key(character_id: int):
        return f"character-detail-{character_id}"

    character_details = {}
    with Cache() as cache:
        missing_character_ids = []
        for character in characters:
            cache_key = make_cache_key(character.id)
            if cache_key in cache:
                character_details[character.id] = cache.get(cache_key)
            else:
                missing_character_ids.append(character.id)

        if missing_character_ids:
            character_details_2 = _fetch_character_details(missing_character_ids)
            for character_id, character_detail in character_details_2.items():
                cache_key = make_cache_key(character_id)
                cache.set(cache_key, character_detail, expire=CHARACTER_DETAIL_TIMEOUT)
            character_details.update(character_details_2)

    for character in characters:
        if character.id in character_details:
            character.update_from_character_details(character_details[character.id])


def _fetch_character_details(
    character_ids: List[int],
) -> Dict[int, CharacterDetail]:
    logger.info("Fetching details for %d characters from ESI.", len(character_ids))
    character_details = {}
    character_api = CharacterApi()
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future_to_character_id = {
            executor.submit(
                character_api.get_characters_character_id, character_id
            ): character_id
            for character_id in character_ids
        }
        for future in concurrent.futures.as_completed(future_to_character_id):
            character_id = future_to_character_id[future]
            try:
                esi_character = future.result()
            except Exception:
                logger.exception("%s generated an exception", character_id)
            else:
                character_detail = CharacterDetail.from_esi_character(esi_character)
                character_details[character_id] = character_detail
    return character_details


def fetch_affiliation_for_characters(
    character_ids: Iterable[int],
) -> List[CharacterAffiliation]:
    """Fetch affiliations for characters from ESI with caching."""

    def make_cache_key(entity_id: int) -> str:
        cache_key = f"character_affiliation_{entity_id}"
        return cache_key

    entity_ids = set(character_ids)
    if not entity_ids:
        return []

    with Cache() as cache:
        entities_map = {}
        for entity_id in entity_ids:
            cache_key = make_cache_key(entity_id)
            if cache_key in cache:
                entities_map[entity_id] = cache.get(cache_key)

        missing_ids = entity_ids - set(entities_map.keys())
        if missing_ids:
            logger.info(
                "Fetching affiliations for %d characters from ESI...", len(missing_ids)
            )
            api = CharacterApi()
            characters: List[
                PostCharactersAffiliation200Ok
            ] = api.post_characters_affiliation(list(missing_ids))
            for record in characters:
                affiliation = CharacterAffiliation.from_esi_record(record)
                entities_map[affiliation.character_id] = affiliation
                cache_key = make_cache_key(affiliation.character_id)
                cache.set(cache_key, affiliation, expire=3600)

    return list(entities_map.values())
