from typing import List

from eve_local_helper.config import DEVELOPER_MODE

TEXT_SAMPLE = """
    Erik Kalkoken\nBaltrom\nWen Gogiko\nEmily Kasenumi\nNaoko Kobayashi\nRosie Dunbar\n
    Chana Reacher\nGarruk IronHand\nArmin Dark\nCasper24\nTitan Ofc\nAnnu\nEkala Hakoke\n
    Captain Jesko\nJerome Skord
"""


def extract_names_from_clipboard(page) -> List[str]:
    if DEVELOPER_MODE:
        text = TEXT_SAMPLE
    else:
        text = page.get_clipboard()
    names = [o.strip() for o in text.splitlines()]
    result = [name for name in names if name]
    return result
