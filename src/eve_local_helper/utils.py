"""General purpose utilities."""

import datetime as dt


def now() -> dt.datetime:
    """Current time in UTC and timezone aware."""
    return dt.datetime.now(dt.timezone.utc)
