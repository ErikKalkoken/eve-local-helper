"""Handle fetching and processing contacts from ESI."""

import concurrent.futures
import enum
import logging
from dataclasses import dataclass
from typing import Dict, List, Union

from esi_client.apis import ContactsApi
from esi_client.models import (
    GetAlliancesAllianceIdContacts200Ok,
    GetCharactersCharacterIdContacts200Ok,
    GetCorporationsCorporationIdContacts200Ok,
)

from eve_local_helper.cache import Cache
from eve_local_helper.tokens import Token

logger = logging.getLogger(__name__)


class ContactType(str, enum.Enum):
    CHARACTER = "character"
    CORPORATION = "corporation"
    ALLIANCE = "alliance"
    FACTION = "faction"

    @classmethod
    def from_esi_contact_type(cls, contact_type: str) -> "ContactType":
        match contact_type:
            case "character":
                return cls.CHARACTER
            case "corporation":
                return cls.CORPORATION
            case "alliance":
                return cls.ALLIANCE
            case "faction":
                return cls.FACTION
        raise ValueError("Invalid contact type")


@dataclass
class Contact:
    """A contact in Eve Online."""

    id: int
    type: ContactType
    standing: float

    @classmethod
    def from_esi_contact(
        cls,
        esi_contact: Union[
            GetAlliancesAllianceIdContacts200Ok,
            GetCharactersCharacterIdContacts200Ok,
            GetCorporationsCorporationIdContacts200Ok,
        ],
    ) -> "Contact":
        return cls(
            id=esi_contact.contact_id,
            type=ContactType.from_esi_contact_type(esi_contact.contact_type),
            standing=esi_contact.standing,
        )


class StandingCategory(str, enum.Enum):
    EXCELLENT = "excellent"
    GOOD = "good"
    NEUTRAL = "neutral"
    BAD = "BAD"
    TERRIBLE = "terrible"

    @classmethod
    def from_standing(cls, standing: float) -> "StandingCategory":
        if standing > 5:
            return cls.EXCELLENT
        if standing > 0:
            return cls.GOOD
        if standing == 0:
            return cls.NEUTRAL
        if standing > -5:
            return cls.BAD
        return cls.TERRIBLE


def fetch_contacts(token: Token) -> Dict[str, Dict[int, Contact]]:
    """Fetch all contacts for given character by token."""
    contacts_by_category = {}
    with concurrent.futures.ThreadPoolExecutor() as executor:
        function_map = {
            "character": fetch_character_contacts,
            "corporation": fetch_corporation_contacts,
            "alliance": fetch_alliance_contacts,
        }
        future_to_contacts = {
            executor.submit(f, token): key for key, f in function_map.items()
        }
        for future in concurrent.futures.as_completed(future_to_contacts):
            key = future_to_contacts[future]
            try:
                contacts = future.result()
            except Exception:
                logger.exception("Exception when trying to fetch contacts for %s", key)
            else:
                contacts_by_category[key] = {o.id: o for o in contacts}

    return contacts_by_category


def determine_contact_standing(
    token: Token, character, contacts: Dict[str, Dict[int, Contact]]
) -> float:
    if (
        token.character_id == character.id
        or token.corporation_id == character.corporation_id
        or token.alliance_id
        and token.alliance_id == character.alliance_id
    ):
        return 10.0

    for category in ["character", "corporation", "alliance"]:
        if character.id in contacts[category]:
            return contacts[category][character.id].standing
        if character.corporation_id in contacts[category]:
            return contacts[category][character.corporation_id].standing
        if character.alliance_id and character.alliance_id in contacts[category]:
            return contacts[category][character.alliance_id].standing

    return 0.0


def fetch_character_contacts(token: Token) -> List[Contact]:
    return _fetch_contacts(
        token, "get_characters_character_id_contacts", token.character_id
    )


def fetch_corporation_contacts(token: Token) -> List[Contact]:
    return _fetch_contacts(
        token, "get_corporations_corporation_id_contacts", token.corporation_id
    )


def fetch_alliance_contacts(token: Token) -> List[Contact]:
    if not token.alliance_id:
        return []
    return _fetch_contacts(
        token, "get_alliances_alliance_id_contacts", token.alliance_id
    )


def _fetch_contacts(token: Token, method_name: str, entity_id: int) -> List[Contact]:
    cache_key = f"{method_name}_{entity_id}"
    with Cache() as cache:
        contacts = cache.get(cache_key)
    if isinstance(contacts, list):
        return contacts
    access_token = token.valid_access_token()
    api = ContactsApi()
    logger.info("Fetching contacts from ESI: %s", method_name)
    esi_contacts = getattr(api, method_name)(entity_id, token=access_token)
    # TODO: add paging
    contacts = [Contact.from_esi_contact(o) for o in esi_contacts]
    with Cache() as cache:
        cache.set(cache_key, contacts, expire=300)
    return contacts
