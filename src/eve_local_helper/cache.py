"""Define same directory for the cache, so it can be reused."""

from diskcache import Cache as BaseCache

from eve_local_helper.dirs import dirs


class Cache(BaseCache):
    def __init__(self, directory=None, *args, **kwargs):
        if not directory:
            directory = dirs.user_cache_dir
        super().__init__(directory, *args, **kwargs)
