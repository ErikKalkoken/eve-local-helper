"""Handle resolving eve entity IDs and names from ESI."""

import logging
from typing import Dict, Iterable, List, Tuple

from esi_client.apis import UniverseApi

from eve_local_helper.cache import Cache

logger = logging.getLogger(__name__)


def resolve_entity_ids(ids: Iterable[int]) -> Dict[int, str]:
    """Resolve entity IDs to names with caching."""

    def make_cache_key(entity_id: int) -> str:
        cache_key = f"entity_id_{entity_id}"
        return cache_key

    entity_ids = set(ids)
    if not entity_ids:
        return {}

    with Cache() as cache:
        entities_map = {}
        for entity_id in entity_ids:
            cache_key = make_cache_key(entity_id)
            if cache_key in cache:
                entities_map[entity_id] = cache.get(cache_key)

        missing_ids = entity_ids - set(entities_map.keys())
        if missing_ids:
            logger.info("Trying to resolve %d ids from ESI...", len(missing_ids))
            universe_api = UniverseApi()
            data = universe_api.post_universe_names(ids=list(missing_ids))
            for record in data:
                entities_map[record.id] = record.name
                cache_key = make_cache_key(record.id)
                cache.set(cache_key, record.name)

    return entities_map


def resolve_names_to_character_ids(names: List[str]) -> Tuple[List[int], List[str]]:
    """Resolve character names to IDs with caching."""

    def make_cache_key(name: str) -> str:
        cache_key = f"character_name_{name}"
        return cache_key

    character_names = set(names)
    if not character_names:
        return [], []

    with Cache() as cache:
        entities_map: Dict[str, int] = {}
        for name in character_names:
            cache_key = make_cache_key(name)
            if cache_key in cache:
                character_id = cache.get(cache_key)
                entities_map[name] = character_id  # type: ignore

        missing_names = character_names - set(entities_map.keys())
        if missing_names:
            logger.info(
                "Trying to resolve %d character names from ESI...", len(missing_names)
            )
            universe_api = UniverseApi()
            data = universe_api.post_universe_ids(list(missing_names))
            try:
                characters = data.characters
            except AttributeError:
                pass
            else:
                for record in characters:
                    entities_map[record.name] = record.id
                    cache_key = make_cache_key(record.name)
                    cache.set(cache_key, record.id)

    unresolved = character_names - set(entities_map.keys())
    return list(entities_map.values()), list(unresolved)
