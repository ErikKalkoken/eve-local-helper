"""Download and cache images from the web.

Needed for the web app, which can not display images from other domains because of CORS.
"""
import base64
import logging
from typing import Optional

import requests

from eve_local_helper.cache import Cache

IMAGES_MAX_AGE_HOURS = 24

logger = logging.getLogger(__name__)


def fetch_image(url: str) -> str:
    """Fetch image for given URL and return as base64 string. With caching."""
    key = "image-" + base64.b64encode(url.encode("utf-8")).decode("utf-8")
    with Cache() as cache:
        data: Optional[bytes] = cache.get(key)  # type: ignore
        if not data:
            logger.info("Download image from url: %s", url)
            r = requests.get(url)
            r.raise_for_status()
            data = r.content
            cache.set(key, data, expire=IMAGES_MAX_AGE_HOURS * 3600)
    encoded = base64.b64encode(data).decode("utf-8")
    return encoded
