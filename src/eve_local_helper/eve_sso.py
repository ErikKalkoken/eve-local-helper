"""Eve Online SSO OAuth."""

import base64
import logging
from secrets import token_urlsafe
from typing import List, Optional, Tuple
from urllib.parse import urlencode, urlparse

import requests
from jose import jwt

from eve_local_helper.config import SSO_CALLBACK_URL, SSO_CLIENT_ID, SSO_CLIENT_SECRET

logger = logging.getLogger(__name__)

AUTHORIZATION_ENDPOINT = "https://login.eveonline.com/v2/oauth/authorize"
TOKEN_ENDPOINT = "https://login.eveonline.com/v2/oauth/token"

STATE = "abc123"

SSO_META_DATA_URL = "https://login.eveonline.com/.well-known/oauth-authorization-server"
JWK_ALGORITHM = "RS256"
JWK_ISSUERS = ("login.eveonline.com", "https://login.eveonline.com")
JWK_AUDIENCE = "EVE Online"


def login_url(scopes: Optional[List[str]] = None) -> Tuple[str, str]:
    """Generate login URL."""
    if not scopes:
        scopes_str = "publicData"
    else:
        scopes_str = " ".join(scopes)
    state = token_urlsafe(16)
    query = {
        "response_type": "code",
        "redirect_uri": SSO_CALLBACK_URL,
        "client_id": SSO_CLIENT_ID,
        "scope": scopes_str,
        "state": state,
    }
    url = f"{AUTHORIZATION_ENDPOINT}?{urlencode(query)}"
    return url, state


def callback_path() -> str:
    results = urlparse(SSO_CALLBACK_URL)
    return results.path


def fetch_new_token(original_state: str, query: dict) -> Tuple[dict, dict]:
    """Fetch new SSO token."""
    if original_state != query["state"]:
        raise ValueError("Invalid state")
    data = {"grant_type": "authorization_code", "code": query["code"]}
    return _fetch_token_from_endpoint(data)


def refresh_token(refresh_token: str) -> Tuple[dict, dict]:
    """Refresh SSO token."""
    data = {"grant_type": "refresh_token", "refresh_token": refresh_token}
    return _fetch_token_from_endpoint(data)


def _fetch_token_from_endpoint(data):
    client_id = SSO_CLIENT_ID
    client_secret = SSO_CLIENT_SECRET
    user_pass = f"{client_id}:{client_secret}"
    basic_auth = base64.urlsafe_b64encode(user_pass.encode("utf-8")).decode()
    headers = {
        "Authorization": f"Basic {basic_auth}",
        "Content-Type": "application/x-www-form-urlencoded",
        "Host": "login.eveonline.com",
    }
    logger.info("Fetching token from %s", TOKEN_ENDPOINT)
    r = requests.post(TOKEN_ENDPOINT, data=data, headers=headers)
    r.raise_for_status()
    data = r.json()
    details = _validate_eve_jwt(data["access_token"])
    return data, details


def _validate_eve_jwt(token: str) -> dict:
    """Validate a JWT access token retrieved from the EVE SSO.

    Args:
        token: A JWT access token originating from the EVE SSO
    Returns:
        The contents of the validated JWT access token if there are no errors
    """
    # fetch JWKs URL from meta data endpoint
    res = requests.get(SSO_META_DATA_URL)
    res.raise_for_status()
    data = res.json()
    try:
        jwks_uri = data["jwks_uri"]
    except KeyError:
        raise RuntimeError(
            f"Invalid data received from the SSO meta data endpoint: {data}"
        ) from None

    # fetch JWKs from endpoint
    res = requests.get(jwks_uri)
    res.raise_for_status()
    data = res.json()
    try:
        jwk_sets = data["keys"]
    except KeyError:
        raise RuntimeError(
            f"Invalid data received from the the jwks endpoint: {data}"
        ) from None

    # pick the JWK with the requested algorithm
    jwk_set = [item for item in jwk_sets if item["alg"] == JWK_ALGORITHM].pop()

    # try to decode the token and validate it against expected values
    # will raise JWT exceptions if decoding fails or expected values do not match
    contents = jwt.decode(
        token=token,
        key=jwk_set,
        algorithms=jwk_set["alg"],
        issuer=JWK_ISSUERS,
        audience=JWK_AUDIENCE,
    )
    return contents
