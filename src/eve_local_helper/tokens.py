"""Handle SSO tokens."""

import datetime as dt
from dataclasses import dataclass
from typing import List, Optional, Set, Tuple

from esi_client.apis import CharacterApi
from esi_client.models import PostCharactersAffiliation200Ok

from eve_local_helper import eve_sso, eveimageserver
from eve_local_helper.cache import Cache
from eve_local_helper.utils import now


@dataclass
class Token:
    CACHE_BASE_KEY = "eve-token"

    character_id: int
    character_name: str
    corporation_id: int
    alliance_id: Optional[int]
    faction_id: Optional[int]
    access_token: str
    refresh_token: str
    token_type: str
    expires_at: dt.datetime
    scopes: List[str]

    def __str__(self) -> str:
        return f"{self.character_id}-{self.character_name}"

    @property
    def is_valid(self) -> bool:
        return self.expires_at > now()

    @property
    def _cache_key(self) -> str:
        return self._cache_key_generic(self.character_id)

    def save(self):
        with Cache() as cache:
            cache.set(self._cache_key, self)

    def refresh(self):
        """Refresh this token."""
        data, content = eve_sso.refresh_token(self.refresh_token)
        new_token = self.from_sso_token(data, content)
        self.refresh_token = new_token.refresh_token
        self.expires_at = new_token.expires_at
        self.access_token = new_token.access_token
        self.scopes = new_token.scopes
        self.save()

    def valid_access_token(self) -> str:
        if not self.is_valid:
            self.refresh()
        return self.access_token

    def delete(self) -> bool:
        with Cache() as cache:
            return cache.delete(self._cache_key)

    def entity_ids(self) -> Set[int]:
        result = {self.character_id, self.corporation_id}
        if self.alliance_id:
            result.add(self.alliance_id)
        return result

    def character_portrait_url(self, size: int) -> str:
        return eveimageserver.character_portrait_url(self.character_id, size)

    @classmethod
    def from_sso_token(cls, sso_token_data: dict, token_content: dict) -> "Token":
        character_id = int(token_content["sub"].split(":")[2])
        expires_at = now() + dt.timedelta(seconds=sso_token_data["expires_in"])
        corporation_id, alliance_id, faction_id = cls.fetch_character_affiliation(
            character_id
        )
        return cls(
            character_name=token_content["name"],
            character_id=character_id,
            corporation_id=corporation_id,
            alliance_id=alliance_id,
            faction_id=faction_id,
            access_token=sso_token_data["access_token"],
            refresh_token=sso_token_data["refresh_token"],
            token_type=sso_token_data["token_type"],
            expires_at=expires_at,
            scopes=token_content["scp"],
        )

    @classmethod
    def _cache_key_generic(cls, character_id: int) -> str:
        return f"{cls.CACHE_BASE_KEY}-{character_id}"

    @classmethod
    def load_one(cls, character_id: int) -> Optional["Token"]:
        with Cache() as cache:
            return cache.get(cls._cache_key_generic(character_id))  # type: ignore

    @classmethod
    def load_all(cls) -> List["Token"]:
        """Load all tokens from cache."""
        with Cache() as cache:
            keys = [k for k in cache if k and str(k).startswith(cls.CACHE_BASE_KEY)]
            tokens = [cache.get(k) for k in keys if k is not None]
            return sorted(tokens, key=lambda o: o.character_name)  # type: ignore

    @staticmethod
    def fetch_character_affiliation(
        character_id: int,
    ) -> Tuple[int, Optional[int], Optional[int]]:
        api = CharacterApi()
        affiliations: List[
            PostCharactersAffiliation200Ok
        ] = api.post_characters_affiliation([character_id])
        if len(affiliations) != 1:
            raise ValueError(
                "Unexpected affiliation response for "
                f"character_id {character_id}: {affiliations}"
            )
        affiliation = affiliations[0]
        try:
            alliance_id = affiliation.alliance_id
        except AttributeError:
            alliance_id = None
        try:
            faction_id = affiliation.faction_id
        except AttributeError:
            faction_id = None
        return affiliation.corporation_id, alliance_id, faction_id
