"""Setup logging and dispatch to chosen app."""

import argparse
import logging
import logging.config
from pathlib import Path

from eve_local_helper import main_app, sso_app
from eve_local_helper.__about__ import __version__
from eve_local_helper.config import logging_config
from eve_local_helper.dirs import dirs

logging.config.dictConfig(logging_config())
logger = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser(
        prog="eve-local-helper", formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "section",
        nargs="?",
        choices=["characters", "main"],
        default="main",
        help="Select the section of the app to start",
    )
    parser.add_argument(
        "--version", action="version", version=f"Eve Local Helper {__version__}"
    )
    parser.add_argument(
        "--folders",
        action="store_true",
        help="Show location of import files like logging, config and cache and exit",
    )
    args = parser.parse_args()

    if args.folders:
        print(f"Logs: {dirs.user_log_dir}")
        print(f"Cache: {dirs.user_cache_dir}")
        print(f"Config: {dirs.user_config_dir} or {Path.cwd()}")
        exit()

    if args.section and args.section == "characters":
        sso_app.start()
    else:
        main_app.start()


if __name__ == "__main__":
    main()
