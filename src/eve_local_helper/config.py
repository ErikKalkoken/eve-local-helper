"""Configuration for the app."""

import configparser
import os
from pathlib import Path

from eve_local_helper.dirs import dirs

CONFIG_FILENAME = "evelocalhelper.ini"


def load_config():
    config = configparser.ConfigParser()
    config["general"] = {
        "log_level_console": "CRITICAL",
        "log_level_file": "INFO",
        "flet_log_level": "fatal",
        "flet_log_to_file": "false",
    }
    config["sso"] = {
        "callback_url": "http://127.0.0.1:8000/sso/callback",
        "client_id": "",
        "client_secret": "",
    }

    cwd_config = Path.cwd() / CONFIG_FILENAME
    user_config = dirs.user_config_path / CONFIG_FILENAME
    files = config.read([cwd_config, user_config])
    if not files:
        print(f"Writing initial config file: {user_config}")
        with user_config.open("w") as fp:
            config.write(fp)
    return config


def logging_config():
    dirs.user_log_path.mkdir(parents=True, exist_ok=True)
    logging = {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "verbose": {
                "format": "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            },
            "simple": {"format": "%(levelname)s %(message)s"},
        },
        "handlers": {
            "log_file": {
                "level": config["general"]["log_level_file"],
                "class": "logging.handlers.RotatingFileHandler",
                "filename": dirs.user_log_path / "evelocalhelper.log",
                "formatter": "verbose",
                "maxBytes": 1024 * 1024 * 5,
                "backupCount": 5,
            },
            "console": {
                "level": config["general"]["log_level_console"],
                "class": "logging.StreamHandler",
                "formatter": "verbose",
            },
        },
        "loggers": {
            "": {
                "handlers": ["log_file", "console"],
                "level": "DEBUG",
            },
        },
    }
    os.environ["FLET_LOG_LEVEL"] = config["general"].get("flet_log_level", "debug")
    os.environ["FLET_LOG_TO_FILE"] = config["general"].get("flet_log_to_file", "false")
    return logging


def has_sso_config() -> bool:
    return bool(SSO_CLIENT_ID) and bool(SSO_CLIENT_SECRET) and bool(SSO_CALLBACK_URL)


config = load_config()

SSO_CLIENT_ID = config["sso"].get("client_id")
SSO_CLIENT_SECRET = config["sso"].get("client_secret")
SSO_CALLBACK_URL = config["sso"].get("callback_url")
DEVELOPER_MODE = config["general"].getboolean("developer_mode", False)
