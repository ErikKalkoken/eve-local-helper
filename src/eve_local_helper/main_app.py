"""Main desktop app."""

import logging
import logging.config
from typing import List, Optional

import flet as ft
from humanize import naturaldelta

from eve_local_helper.characters import fetch_characters_from_names
from eve_local_helper.clipboard import extract_names_from_clipboard
from eve_local_helper.contacts import (
    StandingCategory,
    determine_contact_standing,
    fetch_contacts,
)
from eve_local_helper.eveimageserver import character_portrait_url
from eve_local_helper.local_images import fetch_image
from eve_local_helper.tokens import Token
from eve_local_helper.utils import now

ICON_RENDER_SIZE = 64
ICON_OUTPUT_SIZE = 40
ICON_BORDER_RADIUS = 10
TEXT_SIZE = 16

logger = logging.getLogger(__name__)


class CharacterSelector(ft.UserControl):
    def select_character(self, e):
        if not e.control or not e.control.data:
            return
        token: Token = e.control.data
        self.popup_control.content.foreground_image_url = token.character_portrait_url(  # type: ignore
            size=ICON_RENDER_SIZE
        )
        self.popup_control.tooltip = token.character_name
        self.data = token
        self.update()

    def build(self):
        tokens = Token.load_all()
        if tokens:
            token = tokens[0]
            current_character_portrait_url = token.character_portrait_url(
                size=ICON_RENDER_SIZE
            )
            items = [
                ft.PopupMenuItem(
                    content=ft.Row(
                        [
                            ft.Image(
                                src_base64=fetch_image(
                                    t.character_portrait_url(size=ICON_RENDER_SIZE)
                                ),
                                height=ICON_OUTPUT_SIZE,
                            ),
                            ft.Text(t.character_name),
                        ]
                    ),
                    data=t,
                    on_click=self.select_character,
                )
                for t in tokens
            ]
            tooltip = token.character_name
            disabled = False
        else:
            token = None
            items = []
            tooltip = "No character"
            current_character_portrait_url = character_portrait_url(
                1, size=ICON_RENDER_SIZE
            )
            disabled = True

        self.data = token
        self.popup_control = ft.PopupMenuButton(
            content=ft.CircleAvatar(
                foreground_image_url=current_character_portrait_url
            ),
            items=items,
            tooltip=tooltip,
            disabled=disabled,
        )
        return self.popup_control


class StandingIcon(ft.UserControl):
    def __init__(self, standing: Optional[float], *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._standing = standing

    def build(self):
        if self._standing is not None:
            category = StandingCategory.from_standing(self._standing)
            match category:
                case StandingCategory.EXCELLENT:
                    name = ft.icons.ADD_BOX
                    color = ft.colors.BLUE
                case StandingCategory.GOOD:
                    name = ft.icons.ADD_BOX
                    color = ft.colors.LIGHT_BLUE
                case StandingCategory.NEUTRAL:
                    name = ft.icons.CHECK_BOX_OUTLINE_BLANK
                    color = ft.colors.GREY
                case StandingCategory.BAD:
                    name = ft.icons.INDETERMINATE_CHECK_BOX
                    color = ft.colors.ORANGE
                case StandingCategory.TERRIBLE:
                    name = ft.icons.INDETERMINATE_CHECK_BOX
                    color = ft.colors.RED
                case _:
                    name = ft.icons.QUESTION_MARK
                    color = ft.colors.GREY
        else:
            name = ft.icons.QUESTION_MARK
            color = ft.colors.GREY
        return ft.Icon(name=name, color=color)


class ImagePlusName(ft.UserControl):
    def __init__(self, url: str, name: str, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._url = url
        self._name = name

    def build(self):
        return ft.Row(
            [
                ft.Image(
                    src=self._url,
                    height=ICON_OUTPUT_SIZE,
                    border_radius=ft.border_radius.all(ICON_BORDER_RADIUS),
                ),
                ft.Text(
                    self._name,
                ),
            ]
        )


class ProgressAlert(ft.UserControl):
    def build(self):
        self.dlg = ft.AlertDialog(
            content=ft.Column(
                [ft.Text(""), ft.ProgressRing()],
                height=50,
                horizontal_alignment=ft.CrossAxisAlignment.CENTER,
            )
        )
        return self.dlg

    def show(self, text: str = ""):
        self.set_text(text)
        self.dlg.open = True
        self.update()

    def hide(self):
        self.dlg.open = False
        self.update()

    def set_text(self, text: str):
        self.dlg.content.controls[0].value = text  # type: ignore

    def update_text(self, text: str):
        self.set_text(text)
        self.update()


class CharactersTable(ft.UserControl):
    def build(self):
        return ft.DataTable(
            columns=[
                ft.DataColumn(ft.Text("St.")),
                ft.DataColumn(ft.Text("Name")),
                ft.DataColumn(ft.Text("Corporation")),
                ft.DataColumn(ft.Text("Alliance")),
                ft.DataColumn(ft.Text("Faction")),
                ft.DataColumn(ft.Text("Security")),
                ft.DataColumn(ft.Text("Age")),
            ],
            bgcolor=ft.colors.GREY_900,
            visible=False,
        )

    def data_cell_launch_url(self, e):
        e.page.launch_url(e.control.data)

    def update_rows(self, names: List[str], token: Token, dialog: ProgressAlert):
        dialog.update_text(f"Fetching {len(names)} characters from ESI...")
        characters, unresolved_names = fetch_characters_from_names(names)
        if token:
            dialog.update_text(
                f"Fetching contacts for {token.character_name} from ESI..."
            )
            contacts = fetch_contacts(token)
        else:
            contacts = {}
        dialog.update_text("Processing results...")
        logger.info("Rendering table")
        rows = []
        for character in characters:
            if token and contacts:
                standing = determine_contact_standing(token, character, contacts)
            else:
                standing = None
            cells = [
                ft.DataCell(StandingIcon(standing)),
                ft.DataCell(
                    ImagePlusName(
                        character.character_portrait_url(ICON_RENDER_SIZE),
                        character.name,
                    ),
                    on_tap=self.data_cell_launch_url,
                    data=character.character_zkb_url(),
                ),
                ft.DataCell(
                    ImagePlusName(
                        character.corporation_logo_url(ICON_RENDER_SIZE),
                        character.corporation_name,
                    ),
                    on_tap=self.data_cell_launch_url,
                    data=character.corporation_zkb_url(),
                ),
            ]
            if character.has_alliance:
                cells.append(
                    ft.DataCell(
                        ImagePlusName(
                            character.alliance_logo_url(ICON_RENDER_SIZE),
                            character.alliance_name,
                        ),
                        on_tap=self.data_cell_launch_url,
                        data=character.alliance_zkb_url(),
                    )
                )
            else:
                cells.append(ft.DataCell(ft.Text("")))
            if character.has_faction:
                cells.append(
                    ft.DataCell(
                        ImagePlusName(
                            character.faction_logo_url(ICON_RENDER_SIZE),
                            character.faction_name,
                        ),
                        on_tap=self.data_cell_launch_url,
                        data=character.faction_zkb_url(),
                    )
                )
            else:
                cells.append(ft.DataCell(ft.Text("")))

            if character.security_status:
                security_status = f"{character.security_status:.1f}"
                if character.security_status <= 0:
                    color = "red"
                elif character.security_status >= 0:
                    color = "green"
                else:
                    color = None
                text_control = ft.Text(security_status, color=color)
            else:
                text_control = ft.Text("")
            cells.append(ft.DataCell(text_control))

            if character.birthday:
                age = now() - character.birthday
                age_str = naturaldelta(age)
            else:
                age_str = ""
            cells.append(ft.DataCell(ft.Text(age_str, width=80)))

            rows.append(ft.DataRow(cells=cells))  # type: ignore

        data_table: ft.DataTable = self.controls[0]  # type: ignore
        data_table.rows = rows
        data_table.visible = True
        self.update()
        return unresolved_names


def main(page: ft.Page):
    page.title = "Eve Local Helper"

    def clipboard_clicked(e):
        dlg.show()
        names = extract_names_from_clipboard(page)
        token = character_selector.data
        unresolved_names = data_table.update_rows(names, token, dlg)
        if token:
            table_header_right.value = f"@{token.character_name}"
        dlg.hide()
        table_header.visible = True

        if unresolved_names:
            text = "Could not resolve: " + ", ".join(unresolved_names)
            page.snack_bar = ft.SnackBar(content=ft.Text(text))
            page.snack_bar.open = True

        page.update()

    dlg = ProgressAlert()
    page.dialog = dlg

    character_selector = CharacterSelector()
    table_header_right = ft.Text("", style=ft.TextThemeStyle.TITLE_LARGE)
    page.add(
        ft.Row(
            [
                ft.FilledButton("Paste from clipboard", on_click=clipboard_clicked),
                character_selector,
            ],
            alignment=ft.MainAxisAlignment.END,
        )
    )
    table_header = ft.Row(
        [
            ft.Text("Characters", style=ft.TextThemeStyle.TITLE_LARGE),
            table_header_right,
        ],
        visible=False,
    )
    page.add(table_header)
    data_table = CharactersTable()
    page.add(
        ft.ListView(controls=[data_table], spacing=10, padding=20, expand=True),
    )


def start():
    ft.app(target=main)


if __name__ == "__main__":
    start()
