"""Web app for adding and removing characters via Eve SSO."""

import logging
import os

import flet as ft

from eve_local_helper import eve_sso
from eve_local_helper.config import has_sso_config
from eve_local_helper.local_images import fetch_image
from eve_local_helper.tokens import Token

logger = logging.getLogger(__name__)


class CharacterList(ft.UserControl):
    def delete_character_click(self, e):
        token: Token = e.control.data
        logger.info("Trying to delete token for: %s", token.character_id)
        if token:
            token.delete()
            self.my_control.controls = self.build_items()
            self.update()

    def build_items(self):
        tokens = Token.load_all()
        items = []
        if not tokens:
            items.append(ft.ListTile(title=ft.Text("No characters yet.")))
        else:
            for token in Token.load_all():
                items.append(
                    ft.ListTile(
                        leading=ft.Image(
                            src_base64=fetch_image(token.character_portrait_url(64))
                        ),
                        title=ft.Text(token.character_name),
                        trailing=ft.Icon(ft.icons.DELETE, color=ft.colors.RED),
                        on_click=self.delete_character_click,
                        data=token,
                        tooltip="Click to remove",
                    )
                )
        return items

    def build(self):
        self.my_control = ft.Column(controls=self.build_items(), col={"md": 6})
        return ft.ResponsiveRow([self.my_control])


def main(page: ft.Page):
    page.title = "Eve Local Helper"
    state = ""

    def exit_app(e):
        os.kill(os.getppid(), 9)

    def add_character_click(e):
        nonlocal state

        url, state = eve_sso.login_url(
            [
                "esi-characters.read_contacts.v1",
                "esi-corporations.read_contacts.v1",
                "esi-alliances.read_contacts.v1",
            ]
        )
        page.launch_url(url, web_window_name="_self")

    def route_change(route):
        page.views.clear()
        main_view = ft.View(
            "/",
            [
                ft.AppBar(
                    title=ft.Text("Character management"),
                    bgcolor=ft.colors.SURFACE_VARIANT,
                )
            ],
        )

        main_view.controls.append(CharacterList())
        main_view.controls.append(
            ft.ElevatedButton("Add character", on_click=add_character_click)
        )
        main_view.controls.append(ft.ElevatedButton("Exit", on_click=exit_app))
        page.views.append(main_view)

        query = page.query
        if query and query.path == eve_sso.callback_path():
            logger.info("Received code from SSO endpoint")
            dlg = ft.AlertDialog(title=ft.Text("Fetching token..."))
            page.dialog = dlg
            dlg.open = True
            page.update()

            data, content = eve_sso.fetch_new_token(state, query.to_dict)
            token = Token.from_sso_token(data, content)
            token.save()
            page.snack_bar = ft.SnackBar(
                ft.Text(f"Successfully added new token for {token.character_name}")
            )
            page.snack_bar.open = True
            page.go("/")

        page.update()

    def view_pop(view):
        if len(page.views) > 1:
            page.views.pop()
            top_view = page.views[-1]
        else:
            top_view = page.views[0]
        page.go(top_view.route)

    page.on_route_change = route_change
    page.on_view_pop = view_pop
    page.go(page.route)


def start():
    if not has_sso_config():
        print("ERROR: Incomplete SSO configuration")
        exit(1)
    ft.app(target=main, port=8000, view=ft.WEB_BROWSER)


if __name__ == "__main__":
    start()
