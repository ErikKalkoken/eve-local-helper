"""Define common folders to be used in the app."""

from platformdirs import PlatformDirs

dirs = PlatformDirs("EveLocalHelper")
